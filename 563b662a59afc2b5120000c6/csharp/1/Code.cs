class Arge
{
    public static int NbYear(int p0, double percent, int aug, int p)
    {
        int years = 0;
        double multiplier = percent / 100;
        while (p0 < p)
        {
            p0 = p0 + (int)(p0 * multiplier) + aug;
            years++;
        }
        return years;
    }
}