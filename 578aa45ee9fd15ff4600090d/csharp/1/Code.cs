using System;

public class Kata
{
    public static int[] SortArray(int[] array)
    {
        if (array == null)
            throw new ArgumentNullException(nameof(array));
        if (array.Length < 2)
            return array;

        for (int i = 0; i < array.Length - 1; i++)
        {
            for (int j = i + 1; j < array.Length; j++)
            {
                if (array[i] % 2 == 0 || array[j] % 2 == 0)
                    continue;

                if (array[i] > array[j])
                {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        return array;
    }
}