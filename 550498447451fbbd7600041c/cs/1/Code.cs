using System;
using System.Collections.Generic;
using System.Linq;

class AreTheySame
{
    public static bool comp(int[] a, int[] b)
    {
        if (a == null || b == null)
            return false;
        if (a.Length != b.Length)
            return false;

        List<int> aOrdered = a.OrderBy(Math.Abs).ToList();
        List<int> bOrdered = b.OrderBy(x => x).ToList();

        for (int i = 0; i < aOrdered.Count; i++)
        {
            if (aOrdered[i] * aOrdered[i] != bOrdered[i])
                return false;
        }

        return true;
    }
}