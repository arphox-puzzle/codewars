using System.Collections.Generic;

public class Brace
{
    public static bool validBraces(string braces)
    {
        Stack<char> stack = new Stack<char>();

        foreach (char c in braces)
        {
            switch (c)
            {
                case ')':
                    if (stack.Count == 0 || stack.Pop() != '(')
                        return false;
                    break;
                case ']':
                    if (stack.Count == 0 || stack.Pop() != '[')
                        return false;
                    break;
                case '}':
                    if (stack.Count == 0 || stack.Pop() != '{')
                        return false;
                    break;
                case '(':
                    stack.Push(c);
                    break;
                case '[':
                    stack.Push(c);
                    break;
                case '{':
                    stack.Push(c);
                    break;
            }
        }

        return stack.Count == 0;
    }
}