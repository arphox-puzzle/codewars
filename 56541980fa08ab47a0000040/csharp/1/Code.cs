using System.Linq;

public class Printer
{
    public static string PrinterError(string s)
    {
        int errors = s.Count(ch => !(ch >= 'a' && ch <= 'm'));
        return $"{errors}/{s.Length}";
    }
}