using System.Linq;

public class SplitString
{
    public static string[] Solution(string str)
    {
        if (str.Length % 2 == 1)
            str += '_';

        string[] output = new string[str.Length / 2];
        for (int i = 0; i < str.Length; i += 2)
        {
            output[i / 2] = str[i].ToString() + str[i + 1];
        }

        return output;
    }
}