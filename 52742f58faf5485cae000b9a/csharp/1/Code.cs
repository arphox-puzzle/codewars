using System;
using System.Text;

public class HumanTimeFormat
{
    private const int MinuteSec = 60;
    private const int HourSec = MinuteSec * 60;
    private const int DaySec = HourSec * 24;
    private const int YearSec = DaySec * 365;

    public static string formatDuration(int seconds)
    {
        if (seconds < 0) throw new ArgumentOutOfRangeException(nameof(seconds));
        if (seconds == 0) return "now";

        StringBuilder resultBuilder = new StringBuilder();

        int years = seconds / YearSec;
        if (years > 0)
        {
            seconds -= years * YearSec;
            AppendFormat(resultBuilder, years, "year");
        }

        int days = seconds / DaySec;
        if (days > 0)
        {
            seconds -= days * DaySec;
            AppendFormat(resultBuilder, days, "day");
        }

        int hours = seconds / HourSec;
        if (hours > 0)
        {
            seconds -= hours * HourSec;
            AppendFormat(resultBuilder, hours, "hour");
        }

        int minutes = seconds / MinuteSec;
        if (minutes > 0)
        {
            seconds -= minutes * MinuteSec;
            AppendFormat(resultBuilder, minutes, "minute");
        }

        if (seconds > 0)
        {
            AppendFormat(resultBuilder, seconds, "second");
        }

        FixEnding(resultBuilder);

        return resultBuilder.ToString();
    }

    private static void AppendFormat(StringBuilder builder, int number, string term)
    {
        builder.Append($"{number} {term}");

        if (number > 1)
            builder.Append('s');

        builder.Append(", ");
    }

    private static void FixEnding(StringBuilder resultBuilder)
    {
        resultBuilder.Remove(resultBuilder.Length - 2, 2); // ending with ", "

        // Find last comma
        int lastCommaIndex = -1;
        for (int i = 0; i < resultBuilder.Length; i++)
            if (resultBuilder[i] == ',')
                lastCommaIndex = i;

        if (lastCommaIndex == -1)
            return;

        resultBuilder.Remove(lastCommaIndex, 1);
        resultBuilder.Insert(lastCommaIndex, " and");
    }
}