using System;
using System.Collections.Generic;
using System.Linq;

public class Kata
{
    public static int[] Divisors(int n)
    {
        List<int> divisors = new List<int>((int)Math.Sqrt(n));
        List<int> divisors2 = new List<int>((int)Math.Sqrt(n));
        int max = (int)Math.Sqrt(n);
        for (int i = 2; i <= max; i++)
        {
            if (n % i == 0)
            {
                divisors.Add(i);
                if (n / i != i)
                    divisors2.Add(n / i);
            }
        }
        if (divisors.Count == 0)
            return null;
        else
            return divisors.Concat(((IEnumerable<int>)divisors2).Reverse()).ToArray();
    }
}