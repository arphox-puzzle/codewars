using System.Linq;

public static class Kata
{
    public static int[] ReverseSeq(int n)
    {
        int[] output = new int[n];

        for (int i = 0; i < n; i++)
        {
            output[i] = n - i;
        }

        return output;
    }
}