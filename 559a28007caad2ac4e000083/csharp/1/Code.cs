using System.Numerics;

public class SumFct
{
    public static BigInteger perimeter(BigInteger n)
    {
        return 4 * Fibonacci(n + 3) - 4;
    }

    private static BigInteger Fibonacci(BigInteger n)
    {
        BigInteger a = BigInteger.Zero;
        BigInteger b = BigInteger.One;
        for (BigInteger i = 0; i < n; i++)
        {
            BigInteger temp = a;
            a = b;
            b = temp + b;
        }
        return a;
    }
}