using System;
using System.Text;

public class Accumul
{
    public static String Accum(string s)
    {
        StringBuilder output = new StringBuilder();
        s = s.ToLower();
        for (int i = 0; i < s.Length; i++)
        {
            char letterLowercased = s[i];
            output.Append(char.ToUpper(letterLowercased));
            for (int j = 0; j < i; j++)
            {
                output.Append(letterLowercased);
            }
            if (i < s.Length - 1)
                output.Append('-');
        }

        return output.ToString();
    }
}