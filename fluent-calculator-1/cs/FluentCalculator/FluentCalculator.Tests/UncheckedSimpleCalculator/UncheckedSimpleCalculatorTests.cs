﻿using NUnit.Framework;

namespace FluentCalculator.Tests.UncheckedSimpleCalculator
{
    [TestFixture]
    public sealed class UncheckedSimpleCalculatorTests
    {
        [TestCase("1 + 1", 1 + 1)]
        [TestCase("1 - 3", 1 - 3)]
        [TestCase("1 / 1", 1.0 / 1)]
        [TestCase("1 / 2", 1.0 / 2)]

        [TestCase("6 * 6", 6 * 6)]
        [TestCase("6 + 8", 6 + 8)]
        [TestCase("8 / 2", 8.0 / 2)]
        [TestCase("2 * 2", 2 * 2)]
        [TestCase("2 + 10", 2 + 10)]
        [TestCase("10 * 4", 10 * 4)]
        [TestCase("4 / 2", 4.0 / 2)]
        [TestCase("2 - 6", 2 - 6)]

        [TestCase("6 * 6 + 8", 6 * 6 + 8)]
        [TestCase("6 + 8 / 2", 6 + 8.0 / 2.0)]
        [TestCase("8 / 2 * 2", 8.0 / 2.0 * 2.0)]
        [TestCase("2 * 2 + 10", 2 * 2 + 10)]
        [TestCase("2 + 10 * 4", 2 + 10.0 * 4.0)]
        [TestCase("10 * 4 / 2", 10.0 * 4.0 / 2.0)]
        [TestCase("4 / 2 - 6", 4.0 / 2.0 - 6)]

        [TestCase("6 * 6 + 8 / 2 * 2 + 10 * 4 / 2 - 6", 6.0 * 6.0 + 8.0 / 2.0 * 2.0 + 10.0 * 4.0 / 2.0 - 6.0)]
        [TestCase("0 - 4 * 3 + 2 / 8 * 1 / 9", 0.0 - 4.0 * 3.0 + 2.0 / 8.0 * 1.0 / 9.0)]
        public void Test(string expression, double expectedResult)
        {
            var calculator = UncheckedSimpleCalculatorTestHelper.Parse(expression);
            double actualResult = calculator.CalculateResult();

            Assert.That(actualResult, Is.EqualTo(expectedResult).Within(0.1));
        }
    }
}