﻿namespace FluentCalculatorNamespace
{
    using System;

    public sealed class SimpleCalculator
    {
        private enum ExpectedObject
        {
            Operand,
            Operator
        }

        private ExpectedObject currentlyExpectedObject = ExpectedObject.Operand;
        private readonly UncheckedSimpleCalculator internalCalculator = new UncheckedSimpleCalculator();

        public void AddOperator(char addedOperator)
        {
            if (currentlyExpectedObject != ExpectedObject.Operator)
                throw new InvalidOperationException("Currently accepting only operators.");

            internalCalculator.AddOperator(addedOperator);
            currentlyExpectedObject = ExpectedObject.Operand;
        }

        public void AddOperand(int operand)
        {
            if (currentlyExpectedObject != ExpectedObject.Operand)
                throw new InvalidOperationException("Currently accepting only operands.");

            internalCalculator.AddOperand(operand);
            currentlyExpectedObject = ExpectedObject.Operator;
        }

        public double CalculateResult()
        {
            currentlyExpectedObject = ExpectedObject.Operand;
            return internalCalculator.CalculateResult();
        }
    }
}