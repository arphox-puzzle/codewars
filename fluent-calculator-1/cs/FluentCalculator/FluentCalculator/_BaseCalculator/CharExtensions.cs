﻿namespace FluentCalculatorNamespace
{
    internal static class CharExtensions
    {
        internal static bool HasNotSmallerPrecedenceThan(this char op1, char op2)
        {
            bool isOp1_FirstOrder() => op1 == '*' || op1 == '/';
            bool isOp2_SecondOrder() => op2 == '+' || op2 == '-';

            return isOp1_FirstOrder() || isOp2_SecondOrder();
        }
    }
}