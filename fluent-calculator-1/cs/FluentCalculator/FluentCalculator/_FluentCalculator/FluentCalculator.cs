﻿namespace Kata
{
    using FluentCalculatorNamespace;
    using System;

    public sealed class FluentCalculator
    {
        private readonly SimpleCalculator calculator;
        private readonly OperatorSpecifier operators;

        public FluentCalculator()
        {
            calculator = new SimpleCalculator();

            operators = new OperatorSpecifier(() => calculator.CalculateResult());
            var operands = new OperandSpecifier();

            operators.Operands = operands;
            operands.Operators = operators;

            operators.OperatorAdded += (_, op) => calculator.AddOperator(op);
            operands.OperandAdded += (_, op) => calculator.AddOperand(op);
        }

        #region [ Operands (Values) ]

        public OperatorSpecifier Zero
        {
            get
            {
                calculator.AddOperand(0);
                return operators;
            }
        }

        public OperatorSpecifier One
        {
            get
            {
                calculator.AddOperand(1);
                return operators;
            }
        }

        public OperatorSpecifier Two
        {
            get
            {
                calculator.AddOperand(2);
                return operators;
            }
        }

        public OperatorSpecifier Three
        {
            get
            {
                calculator.AddOperand(3);
                return operators;
            }
        }

        public OperatorSpecifier Four
        {
            get
            {
                calculator.AddOperand(4);
                return operators;
            }
        }

        public OperatorSpecifier Five
        {
            get
            {
                calculator.AddOperand(5);
                return operators;
            }
        }

        public OperatorSpecifier Six
        {
            get
            {
                calculator.AddOperand(6);
                return operators;
            }
        }

        public OperatorSpecifier Seven
        {
            get
            {
                calculator.AddOperand(7);
                return operators;
            }
        }

        public OperatorSpecifier Eight
        {
            get
            {
                calculator.AddOperand(8);
                return operators;
            }
        }

        public OperatorSpecifier Nine
        {
            get
            {
                calculator.AddOperand(9);
                return operators;
            }
        }

        public OperatorSpecifier Ten
        {
            get
            {
                calculator.AddOperand(10);
                return operators;
            }
        }

        public double Result() => calculator.CalculateResult();

        #endregion
    }

    public sealed class OperatorSpecifier
    {
        internal event EventHandler<char> OperatorAdded;
        internal OperandSpecifier Operands { get; set; }

        private Func<double> resultProvider;

        internal OperatorSpecifier(Func<double> resultProvider)
        {
            this.resultProvider = resultProvider;
        }

        public OperandSpecifier Plus
        {
            get
            {
                OperatorAdded?.Invoke(this, '+');
                return Operands;
            }
        }

        public OperandSpecifier Minus
        {
            get
            {
                OperatorAdded?.Invoke(this, '-');
                return Operands;
            }
        }

        public OperandSpecifier Times
        {
            get
            {
                OperatorAdded?.Invoke(this, '*');
                return Operands;
            }
        }

        public OperandSpecifier DividedBy
        {
            get
            {
                OperatorAdded?.Invoke(this, '/');
                return Operands;
            }
        }

        public double Result() => resultProvider();

        public static implicit operator double(OperatorSpecifier op) => op.Result();
    }

    public sealed class OperandSpecifier
    {
        internal event EventHandler<int> OperandAdded;
        internal OperatorSpecifier Operators { get; set; }
        internal OperandSpecifier() { /* Restrict instantiation to public  */ }

        public OperatorSpecifier Zero
        {
            get
            {
                OperandAdded?.Invoke(this, 0);
                return Operators;
            }
        }

        public OperatorSpecifier One
        {
            get
            {
                OperandAdded?.Invoke(this, 1);
                return Operators;
            }
        }

        public OperatorSpecifier Two
        {
            get
            {
                OperandAdded?.Invoke(this, 2);
                return Operators;
            }
        }

        public OperatorSpecifier Three
        {
            get
            {
                OperandAdded?.Invoke(this, 3);
                return Operators;
            }
        }

        public OperatorSpecifier Four
        {
            get
            {
                OperandAdded?.Invoke(this, 4);
                return Operators;
            }
        }

        public OperatorSpecifier Five
        {
            get
            {
                OperandAdded?.Invoke(this, 5);
                return Operators;
            }
        }

        public OperatorSpecifier Six
        {
            get
            {
                OperandAdded?.Invoke(this, 6);
                return Operators;
            }
        }

        public OperatorSpecifier Seven
        {
            get
            {
                OperandAdded?.Invoke(this, 7);
                return Operators;
            }
        }

        public OperatorSpecifier Eight
        {
            get
            {
                OperandAdded?.Invoke(this, 8);
                return Operators;
            }
        }

        public OperatorSpecifier Nine
        {
            get
            {
                OperandAdded?.Invoke(this, 9);
                return Operators;
            }
        }

        public OperatorSpecifier Ten
        {
            get
            {
                OperandAdded?.Invoke(this, 10);
                return Operators;
            }
        }
    }
}