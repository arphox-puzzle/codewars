﻿namespace FluentCalculatorNamespace
{
    using System;
    using Kata;

    static class Program
    {
        static void Main()
        {
            var calc = new UncheckedSimpleCalculator();
            calc.AddOperand(8);
            calc.AddOperator('/');
            calc.AddOperand(2);
            calc.AddOperator('*');
            calc.AddOperand(2);

            double result = calc.CalculateResult();
            Console.WriteLine(result);
            Console.ReadLine();
        }

        private static void AreEqual(double a, double b)
        {
            Console.WriteLine(a == b);
        }
    }
}