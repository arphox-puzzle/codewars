using System.Text;

public class DnaStrand
{
    public static string MakeComplement(string dna)
    {
        StringBuilder output = new StringBuilder(dna.Length);
        foreach (char c in dna)
        {
            switch (c)
            {
                case 'A': output.Append('T'); break;
                case 'T': output.Append('A'); break;
                case 'C': output.Append('G'); break;
                case 'G': output.Append('C'); break;
                default: output.Append(c); break;
            }
        }
        return output.ToString();
    }
}