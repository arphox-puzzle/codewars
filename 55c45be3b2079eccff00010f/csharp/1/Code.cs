using System.Linq;

public static class Kata
{
    public static string Order(string words)
    {
        if (string.IsNullOrEmpty(words))
            return words;

        var ordered = words
            .Split(' ')
            .OrderBy(x => x.ToCharArray().First(char.IsDigit));
            
        // Instead of First(), you can use Single() for better safety but lower performance.

        return string.Join(" ", ordered);
    }
}