using System.Linq;

public class Kata
{
    public static int DuplicateCount(string str)
    {
        return str.ToCharArray()
            .GroupBy(char.ToUpper)
            .Count(g => g.Count() > 1);
    }
}