using System.Linq;

public class Kata
{
    public static bool IsIsogram(string str)
    {
        return str.ToCharArray().GroupBy(char.ToUpper).All(g => g.Count() == 1);
    }
}