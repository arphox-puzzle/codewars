public static class Kata
{
    public static int Solution(int value)
    {
        int sum = 0;

        for (int i = 1; i < value; i++)
        {
            bool alreadyAdded = false;
            if (i % 3 == 0)
            {
                sum += i;
                alreadyAdded = true;
            }
            if (!alreadyAdded && i % 5 == 0)
            {
                sum += i;
            }
        }

        return sum;
    }
}