using System.Text;

public class Kata
{
    public static string[] TowerBuilder(int totalFloors)
    {
        string[] tower = new string[totalFloors];

        for (int floor = 0; floor < totalFloors; floor++)
        {
            tower[floor] = GetLevel(totalFloors, floor);
        }

        return tower;
    }

    private static string GetLevel(int totalFloors, int currentFloor)
    {
        StringBuilder level = new StringBuilder();
        int spacesCount = totalFloors - currentFloor - 1;

        AppendSpaces(level, spacesCount);
        AppendStars(currentFloor, level);
        AppendSpaces(level, spacesCount);

        return level.ToString();
    }

    private static void AppendStars(int currentFloor, StringBuilder level)
    {
        for (int i = 0; i < currentFloor * 2 + 1; i++)
            level.Append('*');
    }

    private static void AppendSpaces(StringBuilder level, int spacesCount)
    {
        for (int i = 0; i < spacesCount; i++)
            level.Append(' ');
    }
}