using System.Collections.Generic;
using System.Linq;

public static class LongestConsecutives
{
    public static string LongestConsec(string[] strarr, int k)
    {
        if (strarr.Length == 0 || k > strarr.Length || k <= 0)
            return "";

        List<int> lengths = strarr.Select(x => x.Length).ToList();

        int maxLength = 0;
        int pos = 0;
        int to = strarr.Length - (k - 1);

        for (int i = 0; i < to; i++)
        {
            int currentLength = lengths.Skip(i).Take(k).Sum();
            if (currentLength > maxLength)
            {
                maxLength = currentLength;
                pos = i;
            }
        }

        return string.Concat(strarr.Skip(pos).Take(k));
    }
}