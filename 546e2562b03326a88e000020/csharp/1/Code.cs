using System.Collections.Generic;
using System.Linq;

public class Kata
{
    public static int SquareDigits(int n)
    {
        IEnumerable<int> squares = n.ToString()
            .Select(ch =>
            {
                int number = int.Parse(ch.ToString());
                return number * number;
            });

        return int.Parse(string.Join("", squares));
    }
}