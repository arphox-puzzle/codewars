using System;
using System.Linq;

public class Kata
{
    public static long QueueTime(int[] customers, int n)
    {
        int[] workers = new int[n];

        foreach (int customer in customers)
        {
            int minValue = workers.Min();
            int minPosition = Array.IndexOf(workers, minValue);
            workers[minPosition] += customer;
        }

        return workers.Max();
    }
}