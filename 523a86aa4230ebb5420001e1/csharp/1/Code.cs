using System.Collections.Generic;
using System.Linq;

public static class Kata
{
    public static List<string> Anagrams(string word, List<string> words)
    {
        word = Sort(word);
        return words.Where(w => Sort(w) == word).ToList();
    }

    private static string Sort(string input)
    {
        return new string(input.OrderBy(x => x).ToArray());
    }
}