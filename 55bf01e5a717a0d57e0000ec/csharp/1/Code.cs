using System.Linq;

public class Persist 
{
    public static int Persistence(long n) 
    {
        if (n < 10)
            return 0;
    
        return 1 + Persistence(n.ToString()
          .Select(x => (int)char.GetNumericValue(x))
          .Aggregate((a, b) => a * b)
        );
    }
}