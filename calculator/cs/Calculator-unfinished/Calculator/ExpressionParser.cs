﻿namespace Calculator
{
    using System.Text;

    internal static class ExpressionParser
    {
        internal static UncheckedSimpleCalculator Parse(string expression)
        {
            var calc = new UncheckedSimpleCalculator();

            StringBuilder currentNumberBuilder = new StringBuilder();
            foreach (char character in expression)
            {
                switch (character)
                {
                    case char digit when char.IsDigit(digit):
                        currentNumberBuilder.Append(digit);
                        break;
                    case char op when op.IsOperator():
                        CreateOperand();
                        calc.AddOperator(op);
                        break;
                    default: continue;
                }
            }

            if (currentNumberBuilder.Length > 0)
                CreateOperand();

            return calc;

            // -------------------------------------------------------------
            void CreateOperand()
            {
                calc.AddOperand(int.Parse(currentNumberBuilder.ToString()));
                currentNumberBuilder.Clear();
            }
        }
    }
}