﻿public class Evaluator
{
    public double Evaluate(string expression)
    {
        Calculator.UncheckedSimpleCalculator calculator = Calculator.ExpressionParser.Parse(expression);
        double result = calculator.CalculateResult();
        return result;
    }
}