﻿using Calculator;
using NUnit.Framework;

[TestFixture]
public class UnitTest
{
    Evaluator Evaluator { get; set; } = new Evaluator();

    [Test]
    [TestCase("1-1", ExpectedResult = 0)]
    [TestCase("1+1", ExpectedResult = 2)]
    [TestCase("1 - 1", ExpectedResult = 0)]
    [TestCase("1* 1", ExpectedResult = 1)]
    [TestCase("1 /1", ExpectedResult = 1)]
    [TestCase("12*-1", ExpectedResult = -12)]
    [TestCase("12* 123/-(-5 + 2)", ExpectedResult = 12.0 * 123.0 / -(-5.0 + 2.0))]
    [TestCase("((80 - (19)))", ExpectedResult = 61)]
    public double TestEvaluation(string expression)
    {
        return Evaluator.Evaluate(expression);
    }
}