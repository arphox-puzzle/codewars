using System;

public class NthSeries
{
    public static string seriesSum(int n)
    {
        double divisor = 1;
        double result = 0;

        for (int i = 0; i < n; i++)
        {
            result += 1 / divisor;
            divisor += 3;
        }

        result = Math.Round(result, 2);
        return $"{result:F2}";
    }
}