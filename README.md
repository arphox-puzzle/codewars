# Codewars

My solutions for https://www.codewars.com/

**Template**
root/url-identifier-of-kata/language

To get `url-identifier-of-kata`, navigate to the Kata's page, and find its identifier.  
Example:  
https://www.codewars.com/kata/calculator/train/csharp  
here, the `url-identifier-of-kata` would be `calculator`.  
  
Also, don't forget to create a `link.txt` file inside the kata's directory which includes the link for the given kata, for easier access.