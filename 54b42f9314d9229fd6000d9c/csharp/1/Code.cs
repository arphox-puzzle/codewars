using System.Collections.Generic;
using System.Text;

public class Kata
{
    public static string DuplicateEncode(string word)
    {
        word = word.ToLower();
        Dictionary<char, int> characterStatistics = new Dictionary<char, int>();

        StringBuilder output = new StringBuilder(word.Length, word.Length);

        foreach (char character in word)
        {
            if (characterStatistics.ContainsKey(character))
                characterStatistics[character]++;
            else
                characterStatistics.Add(character, 1);
        }

        foreach (char character in word)
        {
            if (characterStatistics[character] == 1)
                output.Append('(');
            else
                output.Append(')');
        }

        return output.ToString();
    }
}