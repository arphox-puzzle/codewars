using System.Collections;
using System.Linq;

namespace Solution
{
    class Kata
    {
        public static int binaryArrayToNumber(int[] BinaryArray)
        {
            bool[] values = BinaryArray.Select(x => x == 1).Reverse().ToArray();
            var bitArray = new BitArray(values);

            var result = new int[1];
            bitArray.CopyTo(result, 0);
            return result[0];
        }
    }
}