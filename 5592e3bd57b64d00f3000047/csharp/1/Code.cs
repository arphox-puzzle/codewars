using System;

public class ASum
{

    public static long findNb(long m)
    {
        int i = 0;
        while (m > 0)
        {
            i++;
            m -= (long)Math.Pow(i, 3);
        }
        if (m == 0)
            return i;
        else
            return -1;
    }
}