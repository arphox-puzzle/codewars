public class Kata
{
    public static int[] CountPositivesSumNegatives(int[] input)
    {
        if (input == null || input.Length == 0)
            return new int[0];
        
        int negativeSum = 0;
        int positiveCount = 0;

        foreach (int item in input)
        {
            if (item > 0)
                positiveCount++;
            else if (item < 0)
                negativeSum += item;
        }

        return new int[] { positiveCount, negativeSum };
    }
}