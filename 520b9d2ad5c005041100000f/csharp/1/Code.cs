public class Kata
{
    public static string PigIt(string str)
    {
        string[] parts = str.Split(' ');

        for (int i = 0; i < parts.Length; i++)
        {
            if (parts[i].Length == 1 && !char.IsLetter(parts[i][0]))
                continue;

            parts[i] = parts[i].Substring(1, parts[i].Length - 1) + parts[i][0] + "ay";
        }

        return string.Join(" ", parts);
    }
}