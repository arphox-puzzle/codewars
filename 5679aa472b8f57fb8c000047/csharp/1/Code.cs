public class Kata
{
    public static int FindEvenIndex(int[] arr)
    {
        for (int i = 0; i < arr.Length; i++)
            if (FindSumToLeft(arr, i - 1) == FindSumToRight(arr, i + 1))
                return i;

        return -1;
    }

    private static int FindSumToLeft(int[] array, int fromIndex)
    {
        int sum = 0;
        for (int i = 0; i <= fromIndex; i++)
            sum += array[i];
        return sum;
    }

    private static int FindSumToRight(int[] array, int fromIndex)
    {
        int sum = 0;
        for (int i = fromIndex; i < array.Length; i++)
            sum += array[i];
        return sum;
    }
}