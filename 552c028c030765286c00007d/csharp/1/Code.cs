using System.Collections.Generic;
using System.Linq;

public class IQ
{
    public static int Test(string input)
    {
        List<int> numbers = input.Split(' ').Select(int.Parse).ToList();
        if (numbers.Count(x => x % 2 == 0) == 1)
            return numbers.FindIndex(x => x % 2 == 0) + 1;
        else
            return numbers.FindIndex(x => x % 2 == 1) + 1;
    }
}