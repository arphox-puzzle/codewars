using System;

public class Kata
{
    public static string CreatePhoneNumber(int[] n)
    {
        if (n.Length != 10)
            throw new ArgumentOutOfRangeException(nameof(n), "The array's length should be 10.");

        return $"({n[0]}{n[1]}{n[2]}) {n[3]}{n[4]}{n[5]}-{n[6]}{n[7]}{n[8]}{n[9]}";
    }
}