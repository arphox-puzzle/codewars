using System.Linq;

namespace Solution
{
  class Kata
  {
    public static int find_it(int[] seq) 
    {
      return seq.First(item => seq.Count(i => i == item) % 2 == 1);
    }
  }
}