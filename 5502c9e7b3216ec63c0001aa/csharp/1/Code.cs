using System;
using System.Collections.Generic;
using System.Linq;

public class Kata
{
    public static IEnumerable<string> OpenOrSenior(int[][] data)
    {
        return data.Select(Categorize);
    }

    private static string Categorize(int[] data)
    {
        if (data.Length != 2)
            throw new ArgumentOutOfRangeException(nameof(data), "The input should contain exactly 2 elements");

        if (data[0] >= 55 && data[1] > 7)
            return "Senior";
        else
            return "Open";
    }
}