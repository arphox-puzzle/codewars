using System.Collections.Generic;

public class RomanDecode
{
    private static List<KeyValuePair<string, int>> Values = new List<KeyValuePair<string, int>>()
        {
            new KeyValuePair<string, int>("III", 3),
            new KeyValuePair<string, int>("II", 2),
            new KeyValuePair<string, int>("I", 1),
            new KeyValuePair<string, int>("IV", 4),
            new KeyValuePair<string, int>("V", 5),
            new KeyValuePair<string, int>("IX", 9),
            new KeyValuePair<string, int>("X", 10),
            new KeyValuePair<string, int>("X", 10),
            new KeyValuePair<string, int>("X", 10),
            new KeyValuePair<string, int>("XL", 40),
            new KeyValuePair<string, int>("L", 50),
            new KeyValuePair<string, int>("XC", 90),
            new KeyValuePair<string, int>("C", 100),
            new KeyValuePair<string, int>("C", 100),
            new KeyValuePair<string, int>("C", 100),
            new KeyValuePair<string, int>("CD", 400),
            new KeyValuePair<string, int>("D", 500),
            new KeyValuePair<string, int>("CM", 900),
            new KeyValuePair<string, int>("M", 1000),
            new KeyValuePair<string, int>("M", 1000),
            new KeyValuePair<string, int>("M", 1000),
            new KeyValuePair<string, int>("M", 1000),
        };

    public static int Solution(string roman)
    {
        int sum = 0;
        foreach (KeyValuePair<string, int> value in Values)
        {
            if (roman.EndsWith(value.Key))
            {
                sum += value.Value;
                roman = roman.RemoveFromEnd(value.Key);
            }
        }
        return sum;
    }

}

internal static class Helpers
{
    internal static string RemoveFromEnd(this string s, string suffix)
    {
        if (s.EndsWith(suffix))
            return s.Substring(0, s.Length - suffix.Length);
        else
            return s;
    }
}