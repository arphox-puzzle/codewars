using System;
using System.Collections.Generic;
using System.Linq;

public static class DirReduction
{
    public static string[] dirReduc(string[] rawstrings)
    {
        Stack<string> stack = new Stack<string>();
        foreach (string dir in rawstrings)
        {
            if (!stack.Any())
            {
                stack.Push(dir);
            }
            else
            {
                if (dir.GetOpposite() == stack.Peek())
                    stack.Pop();
                else
                    stack.Push(dir);
            }
        }

        return stack
            .Reverse()
            .Select(d => d.ToString())
            .ToArray();
    }


    private static string GetOpposite(this string direction)
    {
        switch (direction)
        {
            case "NORTH": return "SOUTH";
            case "SOUTH": return "NORTH";
            case "EAST": return "WEST";
            case "WEST": return "EAST";
            default:
                throw new ArgumentOutOfRangeException(nameof(direction), "Unexpected string.");
        }
    }
}