using System;
using System.Linq;

public class DigPow
{
    public static long digPow(int n, int pow)
    {
        int[] numberDigits = n.ToString().ToCharArray().Select(ch => (int)char.GetNumericValue(ch)).ToArray();
        double sum = 0;
        foreach (int digit in numberDigits)
        {
            sum += Math.Pow(digit, pow);
            pow++;
        }
        
        double k = sum / n;
        if (k % 1 == 0)
            return (long)k;
        else
            return -1;
    }
}