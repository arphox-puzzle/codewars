public class Kata
{
  public static string GetMiddle(string s)
  {
  if (s.Length % 2 == 1)
  {
    return s[s.Length / 2].ToString();
  }
  else 
  {
    return s[s.Length/2 - 1].ToString() + s[s.Length/2].ToString();
  }
  }
}