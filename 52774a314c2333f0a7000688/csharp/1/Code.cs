using System;
using System.Collections.Generic;
using System.Linq;

public class Parentheses
{
    private static void Main()
    {
        Console.WriteLine(ValidParentheses("("));
        Console.ReadLine();
    }

    public static bool ValidParentheses(string input)
    {
        Stack<bool> stack = new Stack<bool>();
        foreach (char ch in input)
        {
            switch (ch)
            {
                case '(': stack.Push(true); break;
                case ')':
                {
                    if (!stack.Any())
                        return false;
                    else
                        stack.Pop();
                    break;
                }
            }
        }
        return !stack.Any();
    }
}