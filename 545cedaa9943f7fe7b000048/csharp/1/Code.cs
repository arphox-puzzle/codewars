using System;

public static class Kata
{
    public static bool IsPangram(string input)
    {
        const int numberOfLetters = 'Z' - 'A' + 1;
        const int indexOffset = 'A';
        bool[] occurences = new bool[numberOfLetters];

        foreach (char character in input)
        {
            if (!char.IsLetter(character))
                continue;

            int index = char.ToUpper(character) - indexOffset;
            occurences[index] = true;
        }

        bool isAllCharactersUsed = Array.TrueForAll(occurences, x => x == true);
        return isAllCharactersUsed;
    }
}