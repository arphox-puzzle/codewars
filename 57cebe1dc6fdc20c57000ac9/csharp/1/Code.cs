using System.Linq;

public class Kata
{
    public static int FindShort(string input)
    {
        return input.Split(' ').Min(x => x.Length);
    }
}