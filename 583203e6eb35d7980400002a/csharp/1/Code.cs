using System.Linq;
using System.Text.RegularExpressions;

public static class Kata
{
    private static readonly Regex regex = new Regex(@"^(:|;)(-|~)?(\)|D)$");

    public static int CountSmileys(string[] smileys)
    {
        return smileys.Count(regex.IsMatch);
    }
}