using System.Collections.Generic;

public class Kata
{
    public static int[] DeleteNth(int[] numbers, int n)
    {
        Dictionary<int, int> occurenceTable = new Dictionary<int, int>();
        List<int> output = new List<int>();

        foreach (int currentNumber in numbers)
        {
            int occurences;
            if (occurenceTable.TryGetValue(currentNumber, out occurences))
            {
                if (occurences >= n)
                    continue;
                else
                    occurenceTable[currentNumber] = occurences + 1;
            }
            else
            {
                occurenceTable.Add(currentNumber, 1);
            }

            output.Add(currentNumber);
        }

        return output.ToArray();
    }
}